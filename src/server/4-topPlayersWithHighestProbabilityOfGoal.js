function topPlayersWithHighestProbabilityOfGoal(players) {
  let totalCount;
  let ogCount;

  let goalsPerPlayer = players.reduce((acc, player) => {
    if (player["Event"] != "") {
      let name = player["Player Name"];
      let event = player["Event"];
      totalCount = event.split("G").length - 1;
      ogCount = event.split("OG").length - 1;
      let gCount = totalCount - ogCount;
      if (gCount > 0) {
        if (!acc[name]) {
          acc[name] = gCount;
        } else {
          acc[name] = acc[name] + gCount;
        }
      }
    }
    return acc;
  }, {});

  let matchesPlayedPerPlayer = players.reduce((acc, player) => {
    if (!acc[player["Player Name"]]) {
      acc[player["Player Name"]] = 1;
    } else {
      acc[player["Player Name"]]++;
    }
    return acc;
  }, {});

  let probability = Object.keys(goalsPerPlayer).reduce((acc, key) => {
    let division = goalsPerPlayer[key] / matchesPlayedPerPlayer[key];
    acc[key] = division;
    return acc;
  }, {});

  let probForAllPlayers = Object.entries(probability)
    .sort((a, b) => b[1] - a[1])
    .slice(0, 10);
  let probForAllPlayersObject = Object.fromEntries(probForAllPlayers);
  return probForAllPlayersObject;
}

module.exports = topPlayersWithHighestProbabilityOfGoal;
