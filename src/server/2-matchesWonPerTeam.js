function matchesWonPerTeam(players) {
  let matchesPerTeam = players.reduce((acc, player) => {
    if (player["Home Team Goals"] > player["Away Team Goals"]) {
      if (!acc[player["Home Team Name"]]) {
        acc[player["Home Team Name"]] = 1;
      } else {
        acc[player["Home Team Name"]]++;
      }
    } else if (player["Home Team Goals"] < player["Away Team Goals"]) {
      if (!acc[player["Away Team Name"]]) {
        acc[player["Away Team Name"]] = 1;
      } else {
        acc[player["Away Team Name"]]++;
      }
    }
    return acc;
  }, {});

  return matchesPerTeam;
}

module.exports = matchesWonPerTeam;
