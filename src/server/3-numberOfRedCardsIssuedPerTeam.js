function numberOfRedCardsIssuedPerTeam(matches, players, year) {
  let matchId = new Set();
  matches.filter((match) => {
    if (match["Year"] === year) {
      matchId.add(match["MatchID"]);
    }
  });
  let playersMatchingMatchId = players.filter((player) =>
    matchId.has(player["MatchID"])
  );
  let redCardIssued = playersMatchingMatchId.reduce((acc, player) => {
    let initials = player["Team Initials"];
    let redCard = player["Event"].includes("R");

    if (redCard) {
      if (acc[initials]) {
        acc[initials]++;
      } else {
        acc[initials] = 1;
      }
    }
    return acc;
  }, {});

  return redCardIssued;
}

module.exports = numberOfRedCardsIssuedPerTeam;
