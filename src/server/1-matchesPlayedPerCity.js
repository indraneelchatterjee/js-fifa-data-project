function matchesPlayedPerCity(matches) {
  let cities = matches.map((obj) => {
    if (obj.City != "") {
      return obj.City.trim();
    }
  });
  let matchPerCity = cities.reduce((acc, city) => {
    acc[city] ? acc[city]++ : (acc[city] = 1);
    return acc;
  }, {});
  return matchPerCity;
}

module.exports = matchesPlayedPerCity;
