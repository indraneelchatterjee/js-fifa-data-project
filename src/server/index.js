const fs = require("fs");
let path = require("path");
const csv = require("csvtojson");
let matchesPlayedPerCity = require("./1-matchesPlayedPerCity.js");
let matchesWonPerTeam = require("./2-matchesWonPerTeam.js");
let numberOfRedCardsIssuedPerTeam = require("./3-numberOfRedCardsIssuedPerTeam.js");
let topPlayersWithHighestProbabilityOfGoal = require("./4-topPlayersWithHighestProbabilityOfGoal.js");

// 1st
csv()
  .fromFile(path.join(__dirname, "../data/WorldCupMatches.csv"))
  .then((matches) => {
    const jsonFormat = JSON.stringify(matchesPlayedPerCity(matches));

    fs.writeFile(
      path.join(__dirname, "../public/output/1-matchesPlayedPerCity.json"),
      jsonFormat,
      (err) => {
        if (err) {
          console.error(err);
          return;
        }
        console.log("Data written to file");
      }
    );
  });
// 2nd
csv()
  .fromFile(path.join(__dirname, "../data/WorldCupMatches.csv"))
  .then((players) => {
    const jsonFormat = JSON.stringify(matchesWonPerTeam(players));

    fs.writeFile(
      path.join(__dirname, "../public/output/2-matchesWonPerTeam.json"),
      jsonFormat,
      (err) => {
        if (err) {
          console.error(err);
          return;
        }
        console.log("Data written to file");
      }
    );
  });

//4
csv()
  .fromFile(path.join(__dirname, "../data/WorldCupPlayers.csv"))
  .then((players) => {
    const jsonFormat = JSON.stringify(
      topPlayersWithHighestProbabilityOfGoal(players)
    );

    fs.writeFile(
      path.join(
        __dirname,
        "../public/output/4-topPlayersWithHighestProbabilityOfGoal.json"
      ),
      jsonFormat,
      (err) => {
        if (err) {
          console.error(err);
          return;
        }
        console.log("Data written to file");
      }
    );
  });

//3

const matchesPath = path.join(__dirname, "../data/WorldCupMatches.csv");
const playersPath = path.join(__dirname, "../data/WorldCupPlayers.csv");

csv()
  .fromFile(matchesPath)
  .then((matches) => {
    csv()
      .fromFile(playersPath)
      .then((players) => {
        let year = "2014";
        const jsonFormat = JSON.stringify(
          numberOfRedCardsIssuedPerTeam(matches, players, year)
        );

        fs.writeFile(
          path.join(
            __dirname,
            "../public/output/3-numberOfRedCardsIssuedPerTeam.json"
          ),
          jsonFormat,
          (err) => {
            if (err) {
              console.error(err);
              return;
            }
            console.log("Data written to file");
          }
        );
      });
  });
